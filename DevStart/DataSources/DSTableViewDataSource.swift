//
//  DSTableViewDataSource.swift
//  DevStart
//
//  Created by Krystian Kulawiak on 11/04/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

open class DSTableViewDataSource: NSObject, UITableViewDataSource {
    
    public var viewModel: DSViewModelTableDataSource
    
    public var setupCellBlock: ((UITableView, UITableViewCell, IndexPath) -> Void)?
    
    public required init(viewModel: DSViewModelTableDataSource, viewController: DSTableViewController) {
        self.viewModel = viewModel
        
        super.init()
        
        setupCellBlock = {[weak viewController] tableView, cell, indexPath in
            viewController?.setupCell(tableView, cell: cell, at: indexPath)
        }
    }
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItemsInTableSection(section)
    }
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.identifierForTableItem(atIndexPath: indexPath), for: indexPath)
        cell.backgroundColor = .clear
        setupCellBlock?(tableView,cell,indexPath)
        return cell
    }
    
    open func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfTableSections
    }
}
