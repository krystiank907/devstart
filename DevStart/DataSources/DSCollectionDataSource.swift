//
//  DSCollectionDataSource.swift
//  DevStart
//
//  Created by Krystian Kulawiak on 11/04/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

open class DSCollectionDataSource: NSObject, UICollectionViewDataSource {
    
    public var viewModel: DSViewModelCollectionDataSource
    
    public var setupCellBlock: ((UICollectionView, UICollectionViewCell, IndexPath) -> Void)?
    
    public required init(viewModel: DSViewModelCollectionDataSource, viewController: DSCollectionController) {
        self.viewModel = viewModel
        
        super.init()
        
        setupCellBlock = {[weak viewController] collectionView, cell, indexPath in
            viewController?.setupCell(collectionView, cell: cell, at: indexPath)
        }
    }
    
    open func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfCollectionSections
    }
    
    open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItemsInCollectionSection(section)
    }
    
    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: viewModel.identifierForCollectionItem(atIndexPath: indexPath), for: indexPath)
        setupCellBlock?(collectionView, cell, indexPath)
        return cell
    }
    
    
}

