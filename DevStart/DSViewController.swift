//
//  DSViewController.swift
//  DevStart
//
//  Created by Krystian Kulawiak on 11/04/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import RxSwift

open class DSViewController: UIViewController {
    
    open var disposeBag = DisposeBag()
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        if let viewController = self as? DSViewContronllerProtocol {
            viewController.createViews()
        }
        
    }
    
    open func setupView() {
        view.backgroundColor = UIColor.white
    }
}

