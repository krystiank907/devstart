//
//  UIView+Constraints.swift
//  DevStart
//
//  Created by Krystian Kulawiak on 11/04/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

public extension UIView {
    
    func constraints(equalTo view: UIView, attributes: [NSLayoutConstraint.Attribute] = [.bottom, .top, .leading, .trailing]) -> [NSLayoutConstraint] {
        
        let constraints: [NSLayoutConstraint] = attributes.map({NSLayoutConstraint(item: self, attribute: $0, relatedBy: .equal, toItem: view, attribute: $0, multiplier: 1, constant: 0)})
        
        return constraints
    }
    
    func constraintsEqualToSupperView(_ inset: UIEdgeInsets = .zero) -> [NSLayoutConstraint] {
        
        var constraints: [NSLayoutConstraint] = []
        guard let view = self.superview else { return constraints }
        constraints = [
            NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -inset.bottom),
            NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: inset.top),
            NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: inset.left),
            NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: -inset.right)
        ]
        
        return constraints
    }
    
    func constraints(centerTo view: UIView) -> [NSLayoutConstraint] {
        var constraints: [NSLayoutConstraint] = []
        
        constraints = [
            centerXAnchor.constraint(equalTo: view.centerXAnchor),
            centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ]
        
        return constraints
    }
    
    func constraints(ratio: CGFloat, width: CGFloat) -> [NSLayoutConstraint] {
        var constraints: [NSLayoutConstraint] = []
        
        constraints = [
            widthAnchor.constraint(equalToConstant: width),
            widthAnchor.constraint(equalTo: heightAnchor, multiplier: ratio)
        ]
        
        return constraints
    }
    
    func constraints(align attributes: [NSLayoutConstraint.Attribute], toViews views: [UIView]) -> [NSLayoutConstraint] {
        var constraints: [NSLayoutConstraint] = []
        for view in views {
            for attribute in attributes {
                constraints.append(NSLayoutConstraint(item: self, attribute: attribute, relatedBy: .equal, toItem: view, attribute: attribute, multiplier: 1, constant: 0))
            }
        }
        return constraints
    }
}
