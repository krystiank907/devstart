//
//  ViewController+Ext.swift
//  DevStart
//
//  Created by Krystian Kulawiak on 11/04/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//
import UIKit

extension UIViewController {
    public func showAlert(withTitle title: String, message: String, completion: (() -> Void)? = nil) {
        let ok = UIAlertAction(title: "OK", style: .default) { _ in
            completion?()
        }
        
        showAlertController(title: title, message: message, actions: [ok])
    }
    
    public func showWarnig(withTitle title: String, message: String, completion: (() -> Void)? = nil) {
        let ok = UIAlertAction(title: "Yes", style: .default) { _ in
            completion?()
        }
        
        let cancelAction = UIAlertAction(title: "No", style: .cancel) { _ in
            
        }
        showAlertController(title: title, message: message, actions: [ok, cancelAction])
    }
    
    public func showAlertController(title: String?, message: String?, actions: [UIAlertAction]) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        
        for action in actions {
            alert.addAction(action)
        }
        present(alert, animated: true, completion: nil)
    }
    
    public func presentWithNavigationBar(_ viewController: UIViewController, animated: Bool, completion: (() -> Void)?) {
        let nv = UINavigationController(rootViewController: viewController)
        present(nv, animated: animated, completion: completion)
    }
}

