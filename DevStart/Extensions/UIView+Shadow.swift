//
//  UIView+Shadow.swift
//  DevStart
//
//  Created by Krystian Kulawiak on 11/04/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

extension UIView {
    
    public struct Shadow {
        var shadowRadius: CGFloat
        var shadowOpacity: Float = 0.3
        
        static func shadowForLevel(_ level: Int) -> Shadow{
            let shadowRadius: CGFloat = 2 + CGFloat(level)
            let shadowOpacity: Float = 0.3 + Float(level)/10
            
            return Shadow(shadowRadius: shadowRadius, shadowOpacity: shadowOpacity)
        }
    }
    
    public func setupShadow(_ level: Int) {
        let shadow = Shadow.shadowForLevel(level)
        addShadow(shadowRadius: shadow.shadowRadius, shadowOpacity: shadow.shadowOpacity)
    }
    
    public func addShadow(shadowOffset: CGSize = .zero, shadowRadius: CGFloat = 2, shadowOpacity: Float = 0.3) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = shadowOffset
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = shadowOpacity
        layer.masksToBounds = false
    }
    
    public func removeShadow() {
        layer.shadowColor = nil
        layer.shadowOffset = .zero
        layer.shadowRadius = 0
        layer.shadowOpacity = 0
    }
}

