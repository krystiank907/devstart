//
//  NSObject+Name.swift
//  DevStart
//
//  Created by Krystian Kulawiak on 11/04/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation

public extension NSObject {
    static var className: String {
        return "\(self)"
    }
}
