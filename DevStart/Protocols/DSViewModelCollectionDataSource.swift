//
//  DSViewModelCollectionDataSource.swift
//  DevStart
//
//  Created by Krystian Kulawiak on 11/04/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
import RxRealm

public protocol DSViewModelCollectionDataSource: DSViewModelDataSource {
    var collectionChangeSetObserver: Disposable? { get set }
    var collectionItemIdentifiers: [String: AnyClass] { get }
    var numberOfCollectionSections: Int { get }
    func numberOfItemsInCollectionSection(_ section: Int) -> Int
    func identifierForCollectionItem(atIndexPath indexPath: IndexPath) -> String
    var updateCollectionChange: changeSetUpdateBlock? { get set }
    func observeCollectionChanges()
}

public protocol DSViewModelRealmCollectionDataSource: DSViewModelCollectionDataSource {
    associatedtype CollectionObject: Object
    var collectionRealmDataArray: AnyRealmCollection<CollectionObject>! { get set }
    var collectionItemsCount: Int { get }
    func collectionObjectForIndexPath(_ indexPath: IndexPath) -> CollectionObject?
}

fileprivate struct DestructiveKey {
    static var updateCollectionChange = "DestructiveKey.updateCollectionChange"
    static var collectionChangeSetObserver = "DestructiveKey.collectionChangeSetObserver"
}

public extension DSViewModelCollectionDataSource {
    
    var updateCollectionChange: changeSetUpdateBlock? {
        get {
            if let block = objc_getAssociatedObject(self, &DestructiveKey.updateCollectionChange) as? changeSetUpdateBlock {
                return block
            }
            return nil
        }
        set {
            objc_setAssociatedObject(self, &DestructiveKey.updateCollectionChange, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    var collectionChangeSetObserver: Disposable? {
        get {
            if let observer = objc_getAssociatedObject(self, &DestructiveKey.collectionChangeSetObserver) as? Disposable {
                return observer
            }
            return nil
        }
        set {
            objc_setAssociatedObject(self, &DestructiveKey.collectionChangeSetObserver, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
}

public extension DSViewModelCollectionDataSource {
    func identifierForCollectionItem(atIndexPath indexPath: IndexPath) -> String {
        guard let item = collectionItemIdentifiers.first else {
            fatalError("itemClasses is empty")
        }
        return item.key
    }
    
    var numberOfCollectionSections: Int {
        return 1
    }
    
    func observeCollectionChanges() {}
}


public extension DSViewModelRealmCollectionDataSource {
    
    var collectionItemsCount: Int {
        return collectionRealmDataArray.count
    }
    
    func numberOfItemsInCollectionSection(_ section: Int) -> Int {
        return collectionItemsCount
    }
    
    func collectionObjectForIndexPath(_ indexPath: IndexPath) -> CollectionObject? {
        guard indexPath.row < collectionItemsCount else { return nil}
        return collectionRealmDataArray[indexPath.row]
    }
    
    func observeCollectionChanges() {
        collectionChangeSetObserver?.dispose()
        collectionChangeSetObserver = Observable.changeset(from: collectionRealmDataArray)
            .subscribe(onNext: { [weak self] results, changes in
                if let changes = changes {
                    // it's an update
                    self?.updateCollectionChange?(changes.deleted,changes.inserted,changes.updated)
                } else {
                    // it's the initial data
                }
            })
        collectionChangeSetObserver?.disposed(by: disposeBag)
    }
}

