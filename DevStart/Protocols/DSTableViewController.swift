//
//  DSTableViewController.swift
//  DevStart
//
//  Created by Krystian Kulawiak on 11/04/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

public protocol DSTableViewController: DSViewContronllerProtocol {
    var updateSection: Int { get }
    var tableViewStyle: UITableView.Style { get }
    var separatorStyle: UITableViewCell.SeparatorStyle { get }
    var tableView: UITableView! { get set }
    var tableViewDataSource: UITableViewDataSource! { get set }
    func registerTableCells()
    func setupCell(_ tableView: UITableView, cell: UITableViewCell, at indexPath: IndexPath)
    func updateTableView(deletions: [Int], insertions:[Int], modifications: [Int])
}

public protocol DSTableViewControllerWithViewModel: DSTableViewController {
    associatedtype T: DSViewModelTableDataSource
    var viewModel: T { get set }
    
    func observeTableViewModel()
}


public extension DSTableViewController {
    var tableViewStyle: UITableView.Style {
        return .plain
    }
    
    var separatorStyle: UITableViewCell.SeparatorStyle {
        return .none
    }
    
    var updateSection: Int {
        return 0
    }
    
    func updateTableView(deletions: [Int], insertions:[Int], modifications: [Int]) {
        let newDeletions = deletions.filter({insertions.contains($0) == false })
        let newInsertions = insertions.filter({deletions.contains($0) == false })
        let additionModifications = deletions.filter({insertions.contains($0)})
        let newModifications = additionModifications + modifications
        
        if #available(iOS 11.0, *) {
            tableView.performBatchUpdates({
                tableView.deleteRows(at: newDeletions.map{IndexPath(item: $0, section: updateSection)}, with: .left)
                tableView.insertRows(at: newInsertions.map{IndexPath(item: $0, section: updateSection)}, with: .right)
                tableView.reloadRows(at: newModifications.map{IndexPath(item: $0, section: updateSection)}, with: .fade)
            }, completion: nil)
        } else {
            tableView.beginUpdates()
            tableView.deleteRows(at: newDeletions.map{IndexPath(item: $0, section: updateSection)}, with: .left)
            tableView.insertRows(at: newInsertions.map{IndexPath(item: $0, section: updateSection)}, with: .right)
            tableView.reloadRows(at: newModifications.map{IndexPath(item: $0, section: updateSection)}, with: .fade)
            tableView.endUpdates()
        }
        
    }
}

fileprivate struct DestructiveKey {
    static var tableView = "DestructiveKey.tableView"
    static var tableViewDataSource = "DestructiveKey.tableViewDataSource"
}


public extension DSTableViewController where Self: UIViewController {
    var tableView: UITableView! {
        get {
            if let view = objc_getAssociatedObject(self, &DestructiveKey.tableView) as? UITableView {
                return view
            }
            return nil
        }
        set {
            objc_setAssociatedObject(self, &DestructiveKey.tableView, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    var tableViewDataSource: UITableViewDataSource! {
        get {
            if let dataSource = objc_getAssociatedObject(self, &DestructiveKey.tableViewDataSource) as? UITableViewDataSource {
                return dataSource
            }
            return nil
        }
        set {
            objc_setAssociatedObject(self, &DestructiveKey.tableViewDataSource, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
}


public extension DSTableViewController where Self: DSViewController {
    func setupTableView() {
        if tableView == nil {
            createTableView()
        }
        
        tableView.separatorStyle = separatorStyle
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.dataSource = tableViewDataSource
        tableView.delegate = self as? UITableViewDelegate
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        registerTableCells()
    }
    
    func createTableView() {
        let tableView = UITableView(frame: .zero, style: tableViewStyle)
        
        view.addSubview(tableView)
        self.tableView = tableView
    }
    
    func setupViews() {
        setupTableView()
    }
    
    func createConstraints() {
        
        let constraints: [NSLayoutConstraint] = tableView.constraintsEqualToSupperView()
        
        
        NSLayoutConstraint.activate(constraints)
    }
    
    
}


public extension DSTableViewControllerWithViewModel {
    func registerTableCells() {
        for (identifier, cellType) in viewModel.tableItemIdentifiers {
            tableView.register(cellType, forCellReuseIdentifier: identifier)
        }
    }
    
    func observeTableViewModel() {
        viewModel.updateTableChange = { [weak self] deletions, insertions, modifications in
            
            self?.updateTableView(deletions: deletions, insertions: insertions, modifications: modifications)
            
        }
    }
}

