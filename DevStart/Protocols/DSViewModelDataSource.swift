//
//  DSViewModelDataSource.swift
//  DevStart
//
//  Created by Krystian Kulawiak on 05/12/2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
import RxCocoa


fileprivate struct DestructiveKeys{
    static var disposeBag = "DestructiveKey.disposeBag"
}

public typealias changeSetUpdateBlock = (([Int], [Int], [Int]) -> Void)

public protocol DSViewModelDataSource: class{
    var disposeBag: DisposeBag { get set }
}

public extension DSViewModelDataSource{
    
    var disposeBag: DisposeBag{
        get{
            if let bag = objc_getAssociatedObject(self, &DestructiveKeys.disposeBag) as? DisposeBag{
                return bag
            }
            let bag = DisposeBag()
            objc_setAssociatedObject(self, &DestructiveKeys.disposeBag, bag, .OBJC_ASSOCIATION_RETAIN)
            return bag
        }set{
            objc_setAssociatedObject(self, &DestructiveKeys.disposeBag, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    
}
