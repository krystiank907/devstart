//
//  DSCollectionController.swift
//  DevStart
//
//  Created by Krystian Kulawiak on 11/04/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import Foundation

fileprivate struct DestructiveKey {
    static var collectionView = "DestructiveKey.collectionView"
    static var collectionViewSource = "DestructiveKey.collectionViewSource"
}

public protocol DSCollectionController: DSViewContronllerProtocol  {
    var scrollDirection: UICollectionView.ScrollDirection { get }
    var collectionView: UICollectionView! { get set }
    var collectionViewSource: UICollectionViewDataSource! { get set }
    func registerCollectionCells()
    func setupCell(_ collectionView: UICollectionView, cell: UICollectionViewCell, at indexPath: IndexPath)
    func updateCollection(deletions: [Int], insertions:[Int], modifications: [Int], inSection section: Int)
}

public protocol DSCollectionControllerWithViewModel: DSCollectionController {
    associatedtype T: DSViewModelCollectionDataSource
    var viewModel: T { get set }
    
    func observeCollectionViewModel()
}


public extension DSCollectionController {
    func updateCollection(deletions: [Int], insertions:[Int], modifications: [Int], inSection section: Int) {
        collectionView.performBatchUpdates({
            collectionView.deleteItems(at: deletions.map{IndexPath(item: $0, section: section)})
            collectionView.insertItems(at: insertions.map{IndexPath(item: $0, section: section)})
            collectionView.reloadItems(at: modifications.map{IndexPath(item: $0, section: section)})
        }, completion: nil)
    }
}

public extension DSCollectionController where Self: DSViewController {
    var collectionView: UICollectionView! {
        get {
            if let view = objc_getAssociatedObject(self, &DestructiveKey.collectionView) as? UICollectionView {
                return view
            }
            return nil
        }
        set {
            objc_setAssociatedObject(self, &DestructiveKey.collectionView, newValue, .OBJC_ASSOCIATION_ASSIGN)
        }
    }
    
    var collectionViewSource: UICollectionViewDataSource! {
        get {
            if let dataSource = objc_getAssociatedObject(self, &DestructiveKey.collectionViewSource) as? UICollectionViewDataSource {
                return dataSource
            }
            return nil
        }
        set {
            objc_setAssociatedObject(self, &DestructiveKey.collectionViewSource, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
}

public extension DSCollectionController where Self: DSViewController {
    func setupCollectionView() {
        if collectionView == nil {
            createCollectionView()
        }
        
        collectionView.backgroundColor = .clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.dataSource = collectionViewSource
        collectionView.delegate = self as? UICollectionViewDelegate
        
        registerCollectionCells()
    }
    
    func createCollectionView() {
        
        let collectionFlow = UICollectionViewFlowLayout()
        collectionFlow.scrollDirection = scrollDirection
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionFlow)
        
        view.addSubview(collectionView)
        self.collectionView = collectionView
    }
    
    func setupViews() {
        setupCollectionView()
    }
    
    func createConstraints() {
        
        let constraints = collectionView.constraintsEqualToSupperView()
        
        
        NSLayoutConstraint.activate(constraints)
    }
    
    
}


public extension DSCollectionControllerWithViewModel {
    func registerCollectionCells() {
        for (identifier, cellType) in viewModel.collectionItemIdentifiers {
            collectionView.register(cellType, forCellWithReuseIdentifier: identifier)
        }
    }
    
    func observeCollectionViewModel() {
        viewModel.updateCollectionChange = { [weak self] deletions, insertions, modifications in
            
            self?.updateCollection(deletions: deletions, insertions: insertions, modifications: modifications, inSection: 0)
            
        }
    }
}

