//
//  DSViewControllerProtocol.swift
//  DevStart
//
//  Created by Krystian Kulawiak on 05/12/2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import Foundation

public protocol DSViewContronllerProtocol: class {
    func createViews()
    func setupViews()
    func createConstraints()
}

public extension DSViewContronllerProtocol {
    func createViews() {
        setupViews()
        createConstraints()
    }
}
