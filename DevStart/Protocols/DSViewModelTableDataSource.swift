//
//  DSViewModelTableDataSource.swift
//  DevStart
//
//  Created by Krystian Kulawiak on 11/04/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation
import RealmSwift
import RxRealm
import RxSwift

public protocol DSViewModelTableDataSource: DSViewModelDataSource {
    var tableViewChangeSetObserver: Disposable? { get set }
    
    var tableItemIdentifiers: [String: AnyClass] { get }
    var numberOfTableSections: Int { get }
    func numberOfItemsInTableSection(_ section: Int) -> Int
    func identifierForTableItem(atIndexPath indexPath: IndexPath) -> String
    var updateTableChange: changeSetUpdateBlock? { get set }
    func observeTableChanges()
}

public protocol DSViewModelRealmTableDataSource: DSViewModelTableDataSource {
    associatedtype TableObject: Object
    var tableRealmDataArray: AnyRealmCollection<TableObject>! { get set }
    var tableItemsCount: Int { get }
    func tableObjectForIndexPath(_ indexPath: IndexPath) -> TableObject?
}

fileprivate struct DestructiveKey {
    static var updateTableChange = "DestructiveKey.updateTableChange"
    static var tableViewChangeSetObserver = "DestructiveKey.tableViewChangeSetObserver"
}

public extension DSViewModelTableDataSource {
    
    var updateTableChange: changeSetUpdateBlock? {
        get {
            if let block = objc_getAssociatedObject(self, &DestructiveKey.updateTableChange) as? changeSetUpdateBlock {
                return block
            }
            return nil
        }
        set {
            objc_setAssociatedObject(self, &DestructiveKey.updateTableChange, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    var tableViewChangeSetObserver: Disposable? {
        get {
            if let observer = objc_getAssociatedObject(self, &DestructiveKey.tableViewChangeSetObserver) as? Disposable {
                return observer
            }
            return nil
        }
        set {
            objc_setAssociatedObject(self, &DestructiveKey.tableViewChangeSetObserver, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
}

public extension DSViewModelTableDataSource {
    func identifierForTableItem(atIndexPath indexPath: IndexPath) -> String {
        guard let item = tableItemIdentifiers.first else {
            fatalError("itemClasses is empty")
        }
        return item.key
    }
    
    var numberOfTableSections: Int {
        return 1
    }
    func observeTableChanges() { }
}


public extension DSViewModelRealmTableDataSource {
    
    var tableItemsCount: Int {
        return tableRealmDataArray.count
    }
    
    func numberOfItemsInTableSection(_ section: Int) -> Int {
        return tableItemsCount
    }
    
    func tableObjectForIndexPath(_ indexPath: IndexPath) -> TableObject? {
        guard indexPath.row < tableItemsCount else { return nil}
        return tableRealmDataArray[indexPath.row]
    }
    
    func observeTableChanges() {
        tableViewChangeSetObserver?.dispose()
        tableViewChangeSetObserver = Observable.changeset(from: tableRealmDataArray)
            .subscribe(onNext: { [weak self] results, changes in
                if let changes = changes {
                    // it's an update
                    self?.updateTableChange?(changes.deleted,changes.inserted,changes.updated)
                } else {
                    // it's the initial data
                }
            })
        tableViewChangeSetObserver?.disposed(by: disposeBag)
    }
}

